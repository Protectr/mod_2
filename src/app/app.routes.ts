import { Routes } from '@angular/router';
import { CoursesComponent } from './courses';
import { CourseComponent } from './courses/course';
export const ROUTES: Routes = [
  { path: '',      component: CoursesComponent },
  { path: 'course/:id',  component: CourseComponent }
];
