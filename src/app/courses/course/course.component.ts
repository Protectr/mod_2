import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'single',
  template: `
    <div>
      <h1>Single course content for id: {{id}}</h1>
    </div>
  `
})
export class CourseComponent implements OnInit {
  public id: number;
  public sub: any;
  constructor(private route: ActivatedRoute) {};
  public ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.id = +params['id'];
    });
  }
}
