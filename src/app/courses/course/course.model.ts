export interface Course {
  id: number;
  title: string;
  date_created: string;
  duration: number;
  description: string;
}
