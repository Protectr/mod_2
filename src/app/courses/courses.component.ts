import { Component, OnInit } from '@angular/core';
import { Course } from './course/course.model';
import { CoursesService } from './courses.service';
//

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  selector: 'home',  // <home></home>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  providers: [
    CoursesService
  ],
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styleUrls: [ './courses.component.css' ],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  template: `<div>
    <topform></topform>
    <hr>
    <div>
      <h4>Home component (Courses list)</h4>
      <div class='courses-list' *ngFor = 'let course of courses'>
        <div class='courses-list-item'>
          <div class='courses-list-item-top'>
            <div>
              Duration: {{course.duration}}
            </div>
            <div>
              <a [routerLink] = " ['course', course.id] " class='courses-list-item-link'>
                Details
              </a>
              <a [routerLink] = " ['course', course.id] " class='courses-list-item-link 
mod-edit'>
                Edit
              </a>
              <a [routerLink] = " ['course', course.id] " class='courses-list-item-link 
mod-del'>
                Delete
              </a>
            </div>
          </div>
          <div class="courses-list-item-bottom">
            <div>
              Duration: {{course.duration}} m.
            </div>
            <div>
              Description: {{course.description}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>`
})
export class CoursesComponent implements OnInit {
    public courses: Course[];

    constructor(private service: CoursesService) {}
    public ngOnInit() {
      this.service.getJSON().subscribe(
        (res) => this.courses = res
      );
    };

}
