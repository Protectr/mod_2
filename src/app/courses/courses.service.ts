import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Course } from './course/course.model';

@Injectable ()
export class CoursesService {

  constructor(private http: Http) {
    this.getJSON();
  }

  public getJSON(): Observable<Course[]> {
    return this.http
      .get('app/data/sampledata.json')
      .map((res: Response) => <Course[]> res.json());
  }
};
