import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'topform',
  styleUrls: [ '../courses.component.css' ],
  template: `
    <div class="topform">
      <form class="topform-half" action="">
        <div class="form-row">
          <input type="text" plaseholder="search">
          <button class="btn">Search</button>
        </div>
      </form>
      <div  class="topform-half">
        <button class='courses-list-item-link' (click)="openModal()">
          Create New
        </button>
      </div>
    </div>
  `
})
export class TopFormComponent implements OnInit {
  public dialogRef: MatDialogRef<any>;

  constructor(public dialog: dialogRef ) { }

  public openModal() {
    this.dialogRef = this.dialog.open(TopFormComponent, {
      disableClose: false
    });
  }
}
